# A blog building assignment for CVWO
---

**Name:** Lai Hoang Dung  

**Matriculation number:** A0131125Y    

The website is live at http://louislai.me/blog

The database schema is provided with the file mysql_dump.sql

A provided test user account is **admin** with password **password**
